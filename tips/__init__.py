"""
@author: Julien Zoubian
@organization: CPPM
@copyright: Gnu Public Licence
@contact: zoubian@cppm.in2p3.fr

TIPS initialization.
"""

from Library import *

file = open(__path__[0]+'/VERSION')
__version__ = (file.readline()).strip()
file.close()
__status__ = "Development"
file = open(__path__[0]+'/AUTHORS')
__authors__ = [one.strip() for one in file.readlines()]
file.close()
file = open(__path__[0]+'/CREDITS')
__credits__ = [one.strip() for one in file.readlines()]
file.close()
__license__ = "GNU Public Licence"
__copyright__ = "Copyright 2012, GNU Public Licence"
__maintainer__ = "Julien Zoubian"
__email__ = "zoubian@cppm.in2p3.fr"
